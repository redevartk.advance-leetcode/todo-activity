# [Advance Leetcode] TODO-ACTIVITY

* A comprehensive API designed to handle to-do tasks and activity groups.
* Allows creating, updating, and deleting tasks associated with specific activity groups.
* Supports managing activity groups, setting task priorities, and toggling task statuses.
* Provides functionality for organizing and managing tasks within various activity groups via API endpoints.

## Properties

* Framework 		: NestJS 9.0.0
* Containerization	: Docker
* Database			: MySQL 8.0.30
